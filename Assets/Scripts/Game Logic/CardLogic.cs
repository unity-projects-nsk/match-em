﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardLogic : MonoBehaviour {

    [SerializeField]private string ID;
    [SerializeField] private Sprite cardFront;
    [SerializeField] private Image img;
    [SerializeField] private BoardManager bm;
    [SerializeField] private bool delay = false;

    public Sprite cardBack;

    private void Start()
    {
        img = GetComponent<Image>();
        bm = GameObject.FindGameObjectWithTag("Board").GetComponent<BoardManager>();
    }

    // Checks to see if two selected cards are matching
    public void CheckMatch()
    {
        if (GameManager.Instance.cardGuessOne == null)
        {
            if (img.sprite == cardFront)
                GameManager.Instance.cardGuessOne = this;
        }
        else if (GameManager.Instance.cardGuessTwo == null && GameManager.Instance.cardGuessOne != this)
        {
            if (img.sprite == cardFront)
                GameManager.Instance.cardGuessTwo = this;
        }

        if (GameManager.Instance.cardGuessOne != null && GameManager.Instance.cardGuessTwo != null)
        {
            if (GameManager.Instance.cardGuessOne.GetID() == GameManager.Instance.cardGuessTwo.GetID())
            {
                // Match!
                GameManager.Instance.cardGuessOne.GetComponent<Button>().interactable = false;
                GameManager.Instance.cardGuessTwo.GetComponent<Button>().interactable = false;

                if (CheckBoard())
                {
                    GameManager.Instance.GameOver = true;
                }
            }
            else
            {
                // No Match!
                delay = true;
                StartCoroutine(DelayFlip(0.3f));
            }

            if (delay)
            {
                StartCoroutine(DelayNull(0.4f));
                delay = false;
            }
            else
            {
                GameManager.Instance.cardGuessOne = null;
                GameManager.Instance.cardGuessTwo = null;
            }
        }
    }

    // Checks to see if all cards are not interactable and if so, finishes the game.
    public bool CheckBoard()
    {
        foreach (GameObject obj in bm.cardObjList)
        {
            if (obj.GetComponent<Button>().IsInteractable())
                return false;
        }
        return true;
    }

    // Flips cards over
    public void FlipCard()
    {
        GameManager.Instance.PlayCardFlipSound();

        if (img.sprite == cardBack)
        {
            img.sprite = cardFront;
        }
        else if (img.sprite == cardFront)
        {
            img.sprite = cardBack;
        }
    }

    // Delays the setting of null values to prevent exceptions.
    IEnumerator DelayNull(float time)
    {
        yield return new WaitForSecondsRealtime(time);
        GameManager.Instance.cardGuessOne = null;
        GameManager.Instance.cardGuessTwo = null;
    }

    // Delays the flip of a card back to facing down so player can see the second
    // card they chose.
    IEnumerator DelayFlip(float time)
    {
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = false;
        GameManager.Instance.cardGuessTwo.img.sprite = img.sprite;
        yield return new WaitForSecondsRealtime(time);
        GameManager.Instance.cardGuessOne.img.sprite = cardBack;
        GameManager.Instance.cardGuessTwo.img.sprite = cardBack;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    // Setters and Getters

    public void SetID(string _ID)
    {
        ID = _ID;
    }

    public string GetID()
    {
        return ID;
    }

    public void SetCardFront(Sprite _cardFront)
    {
        cardFront = _cardFront;
    }

    public Sprite GetCardFront()
    {
        return cardFront;
    }
}
