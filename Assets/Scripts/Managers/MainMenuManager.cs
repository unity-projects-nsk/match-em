﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour {

    // Goes to CardLayoutSelection scene on button press.
    public void CardLayoutSelection()
    {
        GameManager.Instance.PlayClickSound();
        SceneManager.LoadScene("CardLayoutSelection");
    }

    // Goes to Options Menu on button press.
    public void OptionsMenu()
    {
        GameManager.Instance.PlayClickSound();
        SceneManager.LoadScene("OptionsMenu");
    }

    // Exits game on button press.
    public void ExitGame()
    {
        GameManager.Instance.PlayClickSound();
        Application.Quit();
    }

}
