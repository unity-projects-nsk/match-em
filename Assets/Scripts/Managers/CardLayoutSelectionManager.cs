﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class CardLayoutSelectionManager : MonoBehaviour {
    
    Color INVALID = Color.red;
    Color VALID = Color.green;

    public TMP_InputField columnInput;
    public TMP_InputField rowInput;
    public TextMeshProUGUI rowColumnProduct;
    public Button playButton;

    void Start ()
    {
        LoadColumnsAndRows();
        columnInput.text = GameManager.Instance.GetCardColumns().ToString();
        rowInput.text = GameManager.Instance.GetCardRows().ToString();
    }

    // Loads PlayerPrefs if they exist.
    void LoadColumnsAndRows()
    {
        if (PlayerPrefs.HasKey("Rows") && PlayerPrefs.HasKey("Columns"))
        {
            GameManager.Instance.SetCardRows(PlayerPrefs.GetInt("Rows"));
            GameManager.Instance.SetCardColumns(PlayerPrefs.GetInt("Columns"));
        }
        else
        {
            GameManager.Instance.SetCardRows(3);
            GameManager.Instance.SetCardColumns(4);
            Debug.Log("Hello");
        }
    }

    // Makes sure the number is between 12 and 104 and even.
    void ValidateProduct()
    {
        int product = int.Parse(rowColumnProduct.text);
        TextMeshProUGUI playButtonText = playButton.GetComponentInChildren<TextMeshProUGUI>();

        if (product % 2 != 0)
        {
            ColorizeTotalCards(rowColumnProduct, rowColumnProduct.GetComponentInParent<Image>(), INVALID);
            playButton.interactable = false;
            playButtonText.text = "Invalid Row and/or Column Sizes";
            GameManager.Instance.SetValid(false);
        }
        else if (product > 104 || product < 12)
        {
            ColorizeTotalCards(rowColumnProduct, rowColumnProduct.GetComponentInParent<Image>(), INVALID);
            playButton.interactable = false;
            playButtonText.text = "Invalid Row and/or Column Sizes";
            GameManager.Instance.SetValid(false);
        }
        else
        {
            ColorizeTotalCards(rowColumnProduct, rowColumnProduct.GetComponentInParent<Image>(), VALID);
            playButton.interactable = true;
            playButtonText.text = "Play Match'em! with " + (product / 2) + " potential matches!";
            GameManager.Instance.SetValid(true);
        }

        if (product == 104)
            playButtonText.text = "Play Match'em! the right way. With two full decks!";
    }

    // Colorizes the total cards field to let user know if it is valid.
    void ColorizeTotalCards(TextMeshProUGUI text, Image image, Color color)
    {
        text.color = color;
        image.color = color;
    }

    // Setters for row anc column

    public void SetColumn()
    {
        try { GameManager.Instance.SetCardColumns(int.Parse(columnInput.text)); }
        catch (System.FormatException)
        {
            GameManager.Instance.SetCardColumns(0);
        }
    }

    public void SetRow()
    {
        try { GameManager.Instance.SetCardRows(int.Parse(rowInput.text)); }
        catch (System.FormatException)
        {
            GameManager.Instance.SetCardRows(0);
        }
    }

    // Get number of cards based on row and column
    public void MultiplyRowAndColumn()
    {
        rowColumnProduct.text = (GameManager.Instance.GetCardColumns() * GameManager.Instance.GetCardRows()).ToString();
        ValidateProduct();
    }

    // Play game 
    public void Play()
    {
        GameManager.Instance.PlayClickSound();
        SceneManager.LoadScene("GameScreen");
    }

    // Saves columns and rows to PlayerPrefs
    public void SaveColumnsAndRows()
    {
        PlayerPrefs.SetInt("Rows", GameManager.Instance.GetCardRows());
        PlayerPrefs.SetInt("Columns", GameManager.Instance.GetCardColumns());
        PlayerPrefs.Save();
    }

    // Returns to main menu on button press
    public void BackToMainMenu()
    {
        GameManager.Instance.PlayClickSound();
        SceneManager.LoadScene("MainMenu");
    }
}
