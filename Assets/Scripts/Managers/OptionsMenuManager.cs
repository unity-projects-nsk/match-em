﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class OptionsMenuManager : MonoBehaviour
{
    bool started = false;

    public Slider SFXSlider;
    public GameObject SFXFillObject;
    public GameObject SFXHandle;
    private TextMeshProUGUI SFXHandleText;

    public Slider MusicSlider;
    public GameObject MusicFillObject;
    public GameObject MusicHandle;
    private TextMeshProUGUI MusicHandleText;

    // Use this for initialization
    void Awake()
    {
        SFXHandleText = SFXHandle.GetComponent<TextMeshProUGUI>();
        MusicHandleText = MusicHandle.GetComponent<TextMeshProUGUI>();
        SFXSlider.value = GameManager.Instance.GetSFXVolume();
        MusicSlider.value = GameManager.Instance.GetMusicVolume();
    }

    private void Start()
    {
        SetSoundSettings();
    }

    // Returns to the main menu on button press.
    public void ReturnToMainMenu()
    {
        GameManager.Instance.PlayClickSound();
        SceneManager.LoadScene("MainMenu");
    }

    // Dyanmically repositions the Text object while the slider moves so it stays
    // well aligned to the slider graphic. Also changes the number of the volume
    // slider to reflect realtime volume normalized between 0 and 100.
    public void ChangeSoundFXLabel()
    {
        GameManager.Instance.PlayClickSound();
        float adjustedPosition = ((SFXSlider.normalizedValue * 12));

        SFXHandleText.SetText((SFXSlider.normalizedValue * 100).ToString("##0"));
        if (SFXSlider.normalizedValue * 100 <= 0)
            SFXFillObject.SetActive(false);
        else
            SFXFillObject.SetActive(true);

        SFXHandle.transform.localPosition = new Vector3(-adjustedPosition, 0);
    }

    // Same as above but for the second slider.
    public void ChangeMusicLabel()
    {
        float adjustedPosition = ((MusicSlider.normalizedValue * 12));

        MusicHandleText.SetText((MusicSlider.normalizedValue * 100).ToString("##0"));
        if (MusicSlider.normalizedValue * 100 <= 0)
            MusicFillObject.SetActive(false);
        else
            MusicFillObject.SetActive(true);

        MusicHandle.transform.localPosition = new Vector3(-adjustedPosition, 0);
    }

    // Sets sound settings based on GameManager.
    public void SetSoundSettings()
    {
        if (started == false)
        {
            SFXSlider.value = GameManager.Instance.GetSFXVolume();
            MusicSlider.value = GameManager.Instance.GetMusicVolume();
        }
        
        GameManager.Instance.SetSFXVolume(SFXSlider.value);
        GameManager.Instance.SetMusicVolume(MusicSlider.value);

        SaveSoundSettings();
    }

    // Saves sound settings to PlayerPrefs.
    private void SaveSoundSettings()
    {
        PlayerPrefs.SetFloat("SFXVolume", GameManager.Instance.GetSFXVolume());
        PlayerPrefs.SetFloat("MusicVolume", GameManager.Instance.GetMusicVolume());
        started = true;
    }
}
