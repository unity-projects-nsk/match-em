﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class PauseMenuManager : MonoBehaviour {

    bool isPaused = false;
    bool optionsMenuOpen = false;
    bool started = false;

    public GameObject pauseMenuCanvas;
    public GameObject optionsMenuCanvas;

    public Slider SFXSlider;
    public GameObject SFXFillObject;
    public GameObject SFXHandle;
    private TextMeshProUGUI SFXHandleText;

    public Slider MusicSlider;
    public GameObject MusicFillObject;
    public GameObject MusicHandle;
    private TextMeshProUGUI MusicHandleText;

    // Use this for initialization
    void Awake () {
        isPaused = false;
        optionsMenuOpen = false;
        pauseMenuCanvas.SetActive(false);
        optionsMenuCanvas.SetActive(false);
        SFXHandleText = SFXHandle.GetComponent<TextMeshProUGUI>();
        MusicHandleText = MusicHandle.GetComponent<TextMeshProUGUI>();
        SFXSlider.value = GameManager.Instance.GetSFXVolume();
        MusicSlider.value = GameManager.Instance.GetMusicVolume();
    }

    private void Start()
    {
        SetSoundSettings();
    }

    // Update is called once per frame
    void Update ()
    {
        // Checks for pause buttons to be pressed.
        if (Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.Escape))
        {
            isPaused = !isPaused;
            PauseGame();
        }
	}

    // Pauses game
    private void PauseGame()
    {
        if (isPaused)
        {
            pauseMenuCanvas.SetActive(true);
            Time.timeScale = 0;
        }
        else if (!isPaused)
        {
            pauseMenuCanvas.SetActive(false);
            optionsMenuCanvas.SetActive(false);
            optionsMenuOpen = false;
            Time.timeScale = 1;
        }
    }

    // Opens the options menu within the pause menu
    public void OpenOptions()
    {
        GameManager.Instance.PlayClickSound();

        optionsMenuOpen = !optionsMenuOpen;
        
        if (optionsMenuOpen)
        {
            pauseMenuCanvas.SetActive(false);
            optionsMenuCanvas.SetActive(true);
        }
        else if (!optionsMenuOpen)
        {
            optionsMenuCanvas.SetActive(false);
            pauseMenuCanvas.SetActive(true);
        }
    }

    // Exits game when button is pressed
    public void ExitGame()
    {
        GameManager.Instance.PlayClickSound();
        Application.Quit();
    }

    // Returns to the main meny when button is pressed
    public void ReturnToMainMenu()
    {
        GameManager.Instance.PlayClickSound();
        SceneManager.LoadScene("MainMenu");
    }

    // Dyanmically repositions the Text object while the slider moves so it stays
    // well aligned to the slider graphic. Also changes the number of the volume
    // slider to reflect realtime volume normalized between 0 and 100.
    public void ChangeSoundFXLabel()
    {
        GameManager.Instance.PlayClickSound();
        float adjustedPosition = ((SFXSlider.normalizedValue * 12));

        SFXHandleText.SetText((SFXSlider.normalizedValue * 100).ToString("##0"));
        if (SFXSlider.normalizedValue * 100 <= 0)
            SFXFillObject.SetActive(false);
        else
            SFXFillObject.SetActive(true);
        
        SFXHandle.transform.localPosition = new Vector3(-adjustedPosition, 0);
    }

    // Same as above but for the second slider
    public void ChangeMusicLabel()
    {
        float adjustedPosition = ((MusicSlider.normalizedValue * 12));

        MusicHandleText.SetText((MusicSlider.normalizedValue * 100).ToString("##0"));
        if (MusicSlider.normalizedValue * 100 <= 0)
            MusicFillObject.SetActive(false);
        else
            MusicFillObject.SetActive(true);
        
        MusicHandle.transform.localPosition = new Vector3(-adjustedPosition, 0);
    }

    // Sets sound variables dependent on the GameManager
    public void SetSoundSettings()
    {
        if (started == false)
        {
            SFXSlider.value = GameManager.Instance.GetSFXVolume();
            MusicSlider.value = GameManager.Instance.GetMusicVolume();
        }
        
        GameManager.Instance.SetSFXVolume(SFXSlider.value);
        GameManager.Instance.SetMusicVolume(MusicSlider.value);

        SaveSoundSettings();
    }

    // Saves sound settings to PlayerPrefs.
    private void SaveSoundSettings()
    {
        PlayerPrefs.SetFloat("SFXVolume", GameManager.Instance.GetSFXVolume());
        PlayerPrefs.SetFloat("MusicVolume", GameManager.Instance.GetMusicVolume());
        started = true;
    }
}
