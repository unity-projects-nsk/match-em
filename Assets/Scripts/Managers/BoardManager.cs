﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class BoardManager : MonoBehaviour {

    public GameObject cardPrefab;

    public List<Card> cardList;
    public List<Card> shuffledList;
    public List<GameObject> cardObjList;

    public TextMeshProUGUI gameOverText;

    private GridLayoutGroup grid;

    private int boardRows;
    private int boardColumns;
    private int numberOfCards;

    private void Awake()
    {
        boardRows = GameManager.Instance.GetCardRows();
        boardColumns = GameManager.Instance.GetCardColumns();
        numberOfCards = boardRows * boardColumns;

        grid = GetComponent<GridLayoutGroup>();
    }

    // Use this for initialization
    void Start () {

        ResizeCards(boardRows);
        ShuffleDeck();
        SpawnCards();

        gameOverText.text = "";
        grid.constraint = GridLayoutGroup.Constraint.FixedRowCount;
        grid.constraintCount = boardRows;
	}

    // Checks to see if GameOver is true
    private void Update()
    {
        if (GameManager.Instance.GameOver)
        {
            StartCoroutine(GameOver());
        }
    }

    // Resizes the board of cards to better fit the screen based on row size
    private void ResizeCards(int boardRows)
    {
        if (boardRows == 8)
            ResizeGridLayout(2.25f);
        else if (boardRows == 7)
            ResizeGridLayout(2.5f);
        else if(boardRows == 6)
            ResizeGridLayout(2.75f);
        else if (boardRows == 5)
            ResizeGridLayout(3f);
        else if (boardRows == 4)
            ResizeGridLayout(3.25f);
        else if (boardRows == 3)
            ResizeGridLayout(3.5f);
    }

    // Resizes the gridlayout by a multiplier so cards fit better.
    private void ResizeGridLayout(float multiplier)
    {
        grid.cellSize = new Vector2(33.0f, 45.0f) * multiplier;
    }

    // Runs game over message
    IEnumerator GameOver()
    {
        gameOverText.text = "<b>Congratulations! You've done it!</b>\n ";
        yield return new WaitForSecondsRealtime(1f);
        gameOverText.text = "<b>Congratulations! You've done it!</b>\nReturning to Main Menu";
        yield return new WaitForSecondsRealtime(0.5f);
        gameOverText.text = "<b>Congratulations! You've done it!</b>\nReturning to Main Menu.";
        yield return new WaitForSecondsRealtime(0.5f);
        gameOverText.text = "<b>Congratulations! You've done it!</b>\nReturning to Main Menu..";
        yield return new WaitForSecondsRealtime(0.5f);
        gameOverText.text = "<b>Congratulations! You've done it!</b>\nReturning to Main Menu...";
        SceneManager.LoadScene("MainMenu");
    }

    // Shuffles the deck of cards so matches are randomized
    void ShuffleDeck()
    {
        for (int i = 0; i < numberOfCards; i++)
        {
            shuffledList.Add(cardList[i]);
        }

        for (int i = 0; i < shuffledList.Count; i++)
        {
            Card temp = shuffledList[i];
            int randomIndex = Random.Range(i, shuffledList.Count);
            shuffledList[i] = shuffledList[randomIndex];
            shuffledList[randomIndex] = temp;
        }
    }

    // Spawns the cards into the gridlayout play area.
    void SpawnCards()
    {

        for (int i = 0; i < shuffledList.Count; i++)
        {
            cardObjList.Add(cardPrefab);
            cardObjList[i] = InflateCard(cardObjList[i], shuffledList[i]);
            cardObjList[i] = Instantiate(cardObjList[i], this.transform);
        }
    }

    // Inflates the button prefab card with all the required information
    // from the card Scriptable Object.
    GameObject InflateCard(GameObject cardObj, Card card)
    {
        Image cardImg = cardObj.GetComponent<Image>();
        CardLogic cardLogic = cardObj.GetComponent<CardLogic>();

        cardObj.name = card.cardImage.name;
        cardLogic.SetID(card.cardID);
        cardLogic.SetCardFront(card.cardImage);
        cardImg.sprite = cardLogic.cardBack;

        return cardObj;
    }
}
