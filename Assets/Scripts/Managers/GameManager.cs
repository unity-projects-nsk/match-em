﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class GameManager : MonoBehaviour {

    [SerializeField] float SoundFXVolume;
    [SerializeField] float MusicVolume;

    int CardRows;
    int CardColumns;
    bool IsValid = false;

    public Sprite cardBack;
    public bool GameOver;

    public AudioMixer MusicMixer;
    public AudioMixer SFXMixer;

    public AudioMixerGroup SFXMixerGroup;

    public CardLogic cardGuessOne = null, cardGuessTwo = null;

    public AudioClip clipCardFlip;
    public AudioClip clipUIClick;

    public AudioSource audioCardFlip;
    public AudioSource audioUIClick;

    #region Singleton
    public static GameManager Instance = null;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }
    #endregion

    private void Start()
    {
        audioCardFlip = AddAudio(clipCardFlip, false, false, SFXMixerGroup);
        audioUIClick = AddAudio(clipUIClick, false, false, SFXMixerGroup);

        if (PlayerPrefs.HasKey("SFXVolume"))
            SoundFXVolume = PlayerPrefs.GetFloat("SFXVolume", -35);
        else
            PlayerPrefs.SetFloat("SFXVolume", -35);

        if (PlayerPrefs.HasKey("MusicVolume"))
            MusicVolume = PlayerPrefs.GetFloat("MusicVolume", -35);
        else
            PlayerPrefs.SetFloat("MusicVolume", -35);

    }

    private void Update()
    {
        MusicMixer.SetFloat("MusicVolume", MusicVolume);
        SFXMixer.SetFloat("SFXVolume", SoundFXVolume);
    }

    // Getters and Setters

    public void SetCardRows(int numberOfCards)
    {
        CardRows = numberOfCards;
    }

    public int GetCardRows()
    {
        return CardRows;
    }

    public void SetCardColumns(int numberOfCards)
    {
        CardColumns = numberOfCards;
    }

    public int GetCardColumns()
    {
        return CardColumns;
    }

    public void SetValid(bool isValid)
    {
        IsValid = isValid;
    }

    public bool GetValid()
    {
        return IsValid;
    }

    public void SetSFXVolume(float volume)
    {
        SoundFXVolume = volume;
    }

    public float GetSFXVolume()
    {
        return SoundFXVolume;
    }

    public void SetMusicVolume(float volume)
    {
        MusicVolume = volume;
    }

    public float GetMusicVolume()
    {
        return MusicVolume;
    }

    // Adds new audio sources and connects them to an AudioMixerGroup
    private AudioSource AddAudio(AudioClip clip, bool loop, bool playAwake, AudioMixerGroup mixerGroup)
    {
        AudioSource newAudio = gameObject.AddComponent<AudioSource>();
        newAudio.clip = clip;
        newAudio.loop = loop;
        newAudio.playOnAwake = playAwake;
        newAudio.outputAudioMixerGroup = mixerGroup;
        return newAudio;
    }

    // Play sound methods to be called from other scripts.

    public void PlayClickSound()
    {
        //audioUIClick.Play();
    }

    public void PlayCardFlipSound()
    {
        audioCardFlip.Play();
    }
}
