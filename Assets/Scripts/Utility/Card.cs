﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Scriptable Object to hold info about individual cards.

[CreateAssetMenu(fileName = "Card")]
public class Card : ScriptableObject {
    public string cardID;
    public Sprite cardImage;
}
